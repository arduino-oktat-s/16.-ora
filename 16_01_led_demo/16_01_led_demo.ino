// ESP8266
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

// Webserver Config
const char *ssid = "Charter Informatika 2019";
const char *password = "charter1";
ESP8266WebServer server ( 80 );

// Neopixel Config
#define LedPIN D5
bool ledState = false;

void setup ( void ) {

  Serial.begin ( 115200 );

  // ##############
  // LED
  pinMode ( LedPIN, OUTPUT );
  
  // #########
  // Webserver
  WiFi.begin ( ssid, password );
  Serial.println ();

  // Wait for connection
  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }

  Serial.println ( "" );
  Serial.print ( "Connected to " );
  Serial.println ( ssid );
  Serial.print ( "IP address: " );
  Serial.println ( WiFi.localIP() );

  if ( MDNS.begin ( "esp8266" ) ) {
    Serial.println ( "MDNS responder started" );
  }

  // what to do with requests
  server.on ( "/", handleRoot );
  server.begin();

  Serial.println ( "HTTP server started" );
}

void loop ( void ) {
  // waiting for a client
  server.handleClient();
}


void handleRoot() {
  Serial.println("Client connected");
  
  // data from the checkbox
  String ledstr = server.arg("l");
  Serial.println("LED: " + ledstr);
  if(ledstr == "")
    LEDoff();
  else if(ledstr == "LED")
    LEDon();

  // building a website
  char temp[5000];
  snprintf ( temp, 5000,

"<!DOCTYPE html>\n<html>\n\
  <head>\n\
    <title>Robotika Pecs LED Controller</title>\n\
  </head>\n\
  <body>\n\
    <h1>Robotika Pecs LED-je</h1>\n\
    \n\
    <form action=\"\" name=\"pick\" method=\"post\">\n\
      <input id=\"led\" type=\"checkbox\" name=\"l\" value=\"LED\" onchange=\"document.forms['pick'].submit();\" %s/>\n\
      <label for=\"led\">LED</label>\n\
    </form>\n\
    \n\
  </body>\
</html>",

    ledState ? "checked" : ""
  );
  server.send ( 200, "text/html", temp );
}

void LEDon() {
  ledState = true;
  digitalWrite(LedPIN, HIGH);
}
void LEDoff() {
  ledState = false;
  digitalWrite(LedPIN, LOW);
}
