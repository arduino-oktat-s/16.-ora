#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

const char *ssid = "Charter Informatika 2019";
const char *password = "charter1";
ESP8266WebServer server ( 80 );

#define NeoPIN D4
#define LedPIN D5
#define swPIN D6
bool ledState = false;
#define NUM_LEDS 8
int brightness = 150;
MDNSResponder mdns;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, NeoPIN, NEO_RGB + NEO_KHZ800);

const int led = 13;

void setup () {
  Serial.begin ( 115200 );
  pinMode(LedPIN, OUTPUT);

  pinMode(swPIN, INPUT_PULLUP);

  Serial.println();
  strip.setBrightness(brightness);
  strip.begin();
  strip.show();
  delay(50);

  WiFi.begin (ssid, password);
  Serial.println ();

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay (500);
    Serial.print (".");
  }

  Serial.print ("\nConnected to ");
  Serial.println(ssid);
  Serial.print ("IP address: ");
  Serial.println (WiFi.localIP());

  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println ("MDNS responder started");
  }

  server.on ( "/", handleRoot );
  server.onNotFound ( handleNotFound );
  server.begin();

  Serial.println ( "HTTP server started" );
}

void loop ( void ) {
  server.handleClient();
}

void handleRoot() {
  Serial.println("Client connected");
  String color = server.arg("c");
  String ledstr = server.arg("l");
  Serial.println("Color: " + color);
  Serial.println("LED: " + ledstr);

  char clr [7];
  color.toCharArray(clr, 7);
  setNeoColor(color);

  if (ledstr == "")
    LEDoff();
  else if (ledstr == "LED")
    LEDon();

  char temp[5000];
  int sec = millis() / 1000;
  int min = sec / 60;
  int hr = min / 60;
  snprintf ( temp, 5000,

             "<!DOCTYPE html>\n<html>\n\
  <head>\n\
    <title>Robotika Pecs NeoPixel Controller</title>\n\
    <style>\
      body { background-color: #cccccc; font-family: Arial; Color: #008; }\
    </style>\n\
    <meta name=\"viewport\" content=\"width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0\" />\n\
  </head>\n\
  <body>\n\
    <h1>Robotika Pecs Neopixele</h1>\n\
    <p>Bekapcsolva: %02d:%02d:%02d</p>\n\
    \n\
    <form action=\"\" name=\"pick\" method=\"post\">\n\
    <input type=\"color\" name=\"c\" value=\"%02d\" onchange=\"document.forms['pick'].submit();\" />\n\
    &nbsp;<span onclick=\"document.forms['pick'].submit();\" style=\"font-size:16pt;\"> CHANGE </span>\n\
    <br>\
    &nbsp;<input type=\"checkbox\" name=\"l\" value=\"LED\" onchange=\"document.forms['pick'].submit();\" %s/>\n\
    <br>\n\
    &nbspSwitch is %s\n\
    </form>\n\
    \n\
  </body>\
</html>",

             hr, min % 60, sec % 60, clr, ledState ? "checked" : "", !digitalRead(swPIN) ? "on" : "off"
           );
  server.send (200, "text/html", temp);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";

  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName (i) + ": " + server.arg(i) + "\n";
  }

  server.send(404, "text/plain", message);
}



void setNeoColor(String value) {
  Serial.print("Setting Neopixel...");
  // converting Hex to Int
  int number = (int) strtol( &value[1], NULL, 16);

  // splitting into three parts
  int r = number >> 16;
  int g = number >> 8 & 0xFF;
  int b = number & 0xFF;

  // DEBUG
  Serial.print("RGB: ");
  Serial.print(r);
  Serial.print(" ");
  Serial.print(g);
  Serial.print(" ");
  Serial.print(b);
  Serial.println(" ");

  for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color(g, r, b));
  }
  strip.show();
}

void LEDon() {
  ledState = true;
  digitalWrite(LedPIN, HIGH);
}

void LEDoff() {
  ledState = false;
  digitalWrite(LedPIN, LOW);
}
